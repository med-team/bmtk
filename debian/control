Source: bmtk
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Shayan Doust <hello@shayandoust.me>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               python3-all,
               python3-setuptools,
               dh-sequence-python3,
               pandoc,
               python3-jsonschema <!nocheck>,
               python3-pandas <!nocheck>,
               python3-numpy <!nocheck>,
               python3-six <!nocheck>,
               python3-h5py <!nocheck>,
               python3-matplotlib <!nocheck>,
               python3-scipy <!nocheck>,
               python3-skimage (>= 0.19.3-6~) <!nocheck>,
               python3-sympy <!nocheck>,
               python3-pytest <!nocheck>,
               python3-neuron (>= 8.2.2-8~) <!nocheck>,
               python3-ipython <!nodoc>,
               python3-sphinx,
               python3-numpydoc,
               python3-flake8,
               python3-nbsphinx,
               python3-mpi4py <!nocheck>,
               python3-memory-profiler <!nocheck> <!nodoc>,
               fonts-bebas-neue,
               libjs-requirejs <!nodoc>
Standards-Version: 4.7.1
Vcs-Browser: https://salsa.debian.org/med-team/bmtk
Vcs-Git: https://salsa.debian.org/med-team/bmtk.git
Homepage: https://github.com/AllenInstitute/bmtk
Rules-Requires-Root: no

Package: python3-bmtk
Architecture: all
Section: python
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-jsonschema,
         python3-pandas,
         python3-numpy,
         python3-six,
         python3-h5py,
         python3-matplotlib,
         python3-scipy,
         python3-skimage,
         python3-sympy,
         fonts-bebas-neue
Recommends: python3-neuron (>= 8.2.2-8~)
Suggests: python3-bmtk-doc,
          python3-bmtk-examples
Description: development package for building, simulating and analysing large-scale networks
 The brain modelling toolkit is a software development package for
 building, simulating and analysing large-scale networks of different
 levels of resolution.

Package: python3-bmtk-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
         libjs-mathjax,
         libjs-requirejs
Recommends: python3-bmtk
Multi-Arch: foreign
Description: documentation for python3-bmtk
 The brain modelling toolkit is a software development package for
 building, simulating and analysing large-scale networks of different
 levels of resolution.
 .
 This package contains documentation meant to accommodate the
 python3-bmtk package.

Package: python3-bmtk-examples
Architecture: all
Section: python
Depends: ${python3:Depends},
         ${misc:Depends}
Recommends: python3-bmtk
Multi-Arch: foreign
Description: example Python files for python3-bmtk
 The brain modelling toolkit is a software development package for
 building, simulating and analysing large-scale networks of different
 levels of resolution.
 .
 This package contains example Python3 scripts meant to accommodate the
 python3-bmtk package.
